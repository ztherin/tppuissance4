function printMenu( e ){
    var parent = $( e ) ;
    var children = $( parent ).children("section") ;
    if( $('div#menu').css('position') === 'relative' ){
        children.css({
            'opacity' : '1',
            'visibility' : 'visible',
            'transform' : 'scaleY(1)',
        }) ;
    }
}

function hiddMenu( e ){
    var parent = $( e ) ;
    var children = $( parent ).children("section") ;
    if( $('div#menu').css('position') === 'relative' ){
        children.css({
            'opacity' : '0',
            'visibility' : 'hidden',
            'transform' : 'scaleY(0)',
        }) ;
    }
}

function clickMenu( e ){
    $( 'div#menu' ).css({
        'margin-left' : '0px'
    });
}

function closeMenu( e ){
    $( 'div#menu' ).css({
        'margin-left' : '100%'
    });
}

function gameTable(){
	this.casel ;
	this.linel ;
	
	this.score_joueur1 = 0 ;
	this.score_joueur2 = 0 ;

	this.reinitialis_score = () => {
		this.score_joueur1 = 0 ;
		this.score_joueur2 = 0 ;

		$( 'span.joueur1_score' ).text( this.score_joueur1 ) ;
		$( 'span.joueur2_score' ).text( this.score_joueur2 ) ;
	}

	this.initialisation  = ( player1 , player2 , joueur = 0 , numberLine = 6 , numberColumn = 7 ) => {
		$( 'div.grid' ).html( '' ) ;
		$( 'span#joueurTour' ).text( player1 ) ;
		
		this.pos = [ [0,0] , [0,1] , [0,2] , [0,3] , [0,4] , [0,5] , [0,6] ] ;
		
		this.nb_boules = 0 ;
		$( 'span#nombreBoule' ).text( this.nb_boules ) ;

		this.joueur = joueur ;
		
		$( 'div.container' ).css({
			'pointer-events' : 'all'
		});
		
		for( var i = 0 ; i < numberLine ; i ++ ){

			$( 'span.joueur1_name' ).text( player1 ) ;
			$( 'span.joueur2_name' ).text( player2 ) ;
		
			this.linel = $( '<div/>' ) ;
			this.linel.attr( 'class' , 'line' ) ;
			
			for( var j = 0 ; j < numberColumn ; j ++ ){
				this.casel = $( '<div/>' ) ;
				this.casel.attr( 'class' , 'case' ) ;
				this.casel.attr( 'id' , 'case-' + i + '-' + j ) ;

				this.casel.on( 'click' , ( event )=>{
					var e = event.target ;
					var l_c = $( e ).attr( 'id' ).split( '-' ) ;
		
					l_c = l_c[ 1 ] + '-' + l_c[ 2 ] ;

					var l = l_c[ 0 ] ;
					var c = l_c[ 2 ] ;
					var nb_aligne = 0 ;

					this.nb_boules ++ ;
					$( 'span#nombreBoule' ).text( this.nb_boules ) ;
					
					if( this.pos[ Number(c) ][ 0 ] < 6 ){

						if( this.joueur === 0 ){
							$( 'div#case-' + this.pos[ Number(c) ][ 0 ] + '-' + c ).append( '<div class="boule0"></div>' ) ;
							
							for( var i = 0 ; i < 7 ; i ++ ){
								
								
								if( $( 'div#case-' + this.pos[ Number(c) ][ 0 ] + '-' + i ).children('div').attr( 'class' ) === 'boule0'){
									nb_aligne ++ ;
									if( nb_aligne === 4 ){
										this.score_joueur1 ++ ;
										$( 'span.joueur1_score' ).text( this.score_joueur1 ) ;
										alert( player1 + " a gagner. \nBravo!!!!" ) ;
										
										$( 'div.container' ).css({
											'pointer-events' : 'none'
										});
										break ;
									}
								}

								else
									nb_aligne = 0 ;
							
							}

							nb_aligne = 0 ;

							if( nb_aligne !== 4 ){
								for( var i = 0 ; i < 6 ; i ++ ){
								
									
									if( $( 'div#case-' + i + '-' + this.pos[ Number(c) ][ 1 ] ).children('div').attr( 'class' ) === 'boule0' ){
										nb_aligne ++ ;
										if( nb_aligne === 4 ){
											this.score_joueur1 ++ ;
											$( 'span.joueur1_score' ).text( this.score_joueur1 ) ;
											alert( player1 + " a gagner. \nBravo!!!!" ) ;
											
											$( 'div.container' ).css({
												'pointer-events' : 'none',
											});

											//document.getElementById('ctn').style.pointerEvents = 'none' ;
											break ;
										}
									}

									else
										nb_aligne = 0 ;
								
								}

								nb_aligne = 0 ;
							}

							if( nb_aligne !== 4 ){
								var l_d = 0 ;
								var c_d = 0 ;
								for( var i = 0 ; i < 7 ; i ++ ){
								
									if( this.pos[ Number(c) ][ 0 ] - i >= 0 && this.pos[ Number(c) ][ 1 ] + i <= 6 ){

										l_d = this.pos[ Number(c) ][ 0 ] - i ;
										c_d = this.pos[ Number(c) ][ 1 ] + i ;
									}

								}

								for( var i = 0 ; i < 7 ; i ++ ){
									if( l_d + i <= 5 && c_d - i >= 0 ){
										if( $( 'div#case-' + ( l_d + i ) + '-' + ( c_d - i ) ).children('div').attr( 'class' ) === 'boule0' ){
											nb_aligne ++ ;
											if( nb_aligne === 4 ){
												this.score_joueur1 ++ ;
												$( 'span.joueur1_score' ).text( this.score_joueur1 ) ;
												alert( player1 + " a gagner. \nBravo!!!!" ) ;
												
												$( 'div.container' ).css({
													'pointer-events' : 'none',
												});

												//document.getElementById('ctn').style.pointerEvents = 'none' ;
												break ;
											}
										}

										else
											nb_aligne = 0 ;
									}
								}

								nb_aligne = 0 ;

							}

							if( nb_aligne !== 4 ){
								var l_d = 0 ;
								var c_d = 0 ;
								for( var i = 0 ; i < 7 ; i ++ ){
								
									if( this.pos[ Number(c) ][ 0 ] - i >= 0 && this.pos[ Number(c) ][ 1 ] - i >= 0 ){

										l_d = this.pos[ Number(c) ][ 0 ] - i ;
										c_d = this.pos[ Number(c) ][ 1 ] - i ;
									}

								}

								for( var i = 0 ; i < 7 ; i ++ ){
									if( l_d + i <= 5 && c_d + i <= 6 ){
										if( $( 'div#case-' + ( l_d + i ) + '-' + ( c_d + i ) ).children('div').attr( 'class' ) === 'boule0' ){
											nb_aligne ++ ;
											if( nb_aligne === 4 ){
												this.score_joueur1 ++ ;
												$( 'span.joueur1_score' ).text( this.score_joueur1 ) ;
												alert( player1 + " a gagner. \nBravo!!!!" ) ;
												
												$( 'div.container' ).css({
													'pointer-events' : 'none',
												});

												//document.getElementById('ctn').style.pointerEvents = 'none' ;
												break ;
											}
										}

										else
											nb_aligne = 0 ;
									}
								}

								nb_aligne = 0 ;

							}

							nb_aligne = 0 ;
							$( 'span#joueurTour' ).text( player2 ) ;
							
							this.joueur = 1 ;
						}
						
						else if( this.joueur === 1 ){
							$( 'div#case-' + this.pos[ Number(c) ][ 0 ] + '-' + c ).append( '<div class="boule1"></div>' ) ;
							for( var i = 0 ; i < 7 ; i ++ ){
								
								
								if( $( 'div#case-' + this.pos[ Number(c) ][ 0 ] + '-' + i ).children('div').attr( 'class' ) === 'boule1'){
									nb_aligne ++ ;
									if( nb_aligne === 4 ){
										this.score_joueur2 ++ ;
										$( 'span.joueur2_score' ).text( this.score_joueur2 ) ;
										alert( player2 + " a gagner. \nBravo!!!!" ) ;
										
										$( 'div.container' ).css({
											'pointer-events' : 'none'
										});
										break ;
									}
								}

								else
									nb_aligne = 0 ;
							
							}
							nb_aligne = 0 ;

							if( nb_aligne !== 4 ){
								for( var i = 0 ; i < 6 ; i ++ ){
								
									
									if( $( 'div#case-' + i + '-' + this.pos[ Number(c) ][ 1 ] ).children('div').attr( 'class' ) === 'boule1' ){
										nb_aligne ++ ;
										if( nb_aligne === 4 ){
											this.score_joueur2 ++ ;
											$( 'span.joueur2_score' ).text( this.score_joueur2 ) ;
											alert( player2 + " a gagner. \nBravo!!!!" ) ;
											
											$( 'div.container' ).css({
												'pointer-events' : 'none',
											});

											//document.getElementById('ctn').style.pointerEvents = 'none' ;
											break ;
										}
									}

									else
										nb_aligne = 0 ;
								
								}
								nb_aligne = 0 ;
							}

							if( nb_aligne !== 4 ){
								var l_d = 0 ;
								var c_d = 0 ;
								for( var i = 0 ; i < 7 ; i ++ ){
								
									if( this.pos[ Number(c) ][ 0 ] - i >= 0 && this.pos[ Number(c) ][ 1 ] + i <= 6 ){

										l_d = this.pos[ Number(c) ][ 0 ] - i ;
										c_d = this.pos[ Number(c) ][ 1 ] + i ;
									}

								}

								for( var i = 0 ; i < 7 ; i ++ ){
									if( l_d + i <= 5 && c_d - i >= 0 ){
										if( $( 'div#case-' + ( l_d + i ) + '-' + ( c_d - i ) ).children('div').attr( 'class' ) === 'boule1' ){
											nb_aligne ++ ;
											if( nb_aligne === 4 ){
												this.score_joueur2 ++ ;
												$( 'span.joueur2_score' ).text( this.score_joueur2 ) ;
												alert( player2 + " a gagner. \nBravo!!!!" ) ;
												
												$( 'div.container' ).css({
													'pointer-events' : 'none',
												});

												//document.getElementById('ctn').style.pointerEvents = 'none' ;
												break ;
											}
										}

										else
											nb_aligne = 0 ;
									}
								}
								nb_aligne = 0 ;
							
							}

							if( nb_aligne !== 4 ){
								var l_d = 0 ;
								var c_d = 0 ;
								for( var i = 0 ; i < 7 ; i ++ ){
								
									if( this.pos[ Number(c) ][ 0 ] - i >= 0 && this.pos[ Number(c) ][ 1 ] - i >= 0 ){

										l_d = this.pos[ Number(c) ][ 0 ] - i ;
										c_d = this.pos[ Number(c) ][ 1 ] - i ;
									}

								}

								for( var i = 0 ; i < 7 ; i ++ ){
									if( l_d + i <= 5 && c_d + i <= 6 ){
										if( $( 'div#case-' + ( l_d + i ) + '-' + ( c_d + i ) ).children('div').attr( 'class' ) === 'boule1' ){
											nb_aligne ++ ;

											if( nb_aligne === 4 ){
												this.score_joueur2 ++ ;
												$( 'span.joueur2_score' ).text( this.score_joueur2 ) ;
												alert( player2 + " a gagner. \nBravo!!!!" ) ;
												
												$( 'div.container' ).css({
													'pointer-events' : 'none',
												});

												//document.getElementById('ctn').style.pointerEvents = 'none' ;
												break ;
											}
										}

										else
											nb_aligne = 0 ;
									}
								}
								nb_aligne = 0 ;

							}

							nb_aligne = 0 ;
							$( 'span#joueurTour' ).text( player1 ) ;
							this.joueur = 0 ;
						}

						this.pos[ Number(c) ][ 0 ] = this.pos[ Number(c) ][ 0 ] + 1 ;
					}
				});
				
				this.linel.append( this.casel ) ;
			}

			$( 'div.grid' ).append( this.linel ) ;

		}
	};

}

var game_table = new gameTable() ;

$( 'input#player1' ).on( 'click' , function( event ){
	var joueur = Number( $( event.target ).val() ) ;
	var player1 = $( 'input#joueur1_input' ).val() ;
	var player2 = $( 'input#joueur2_input' ).val() ;
	
	game_table.initialisation( player1 , player2 , joueur ) ;
	
	$( 'div.whoStarted' ).css({
		'opacity' : '0',
		'transform' : 'scale(0)',
	});

	setTimeout(function() {
		$( 'div.whoStarted' ).css({
			'z-index' : '-10000' ,
		});
	}, 500);
});

$( 'input#player2' ).on( 'click' , function( event ){
	var joueur = Number( $( event.target ).val() ) ;
	var player1 = $( 'input#joueur1_input' ).val() ;
	var player2 = $( 'input#joueur2_input' ).val() ;

	game_table.initialisation( player1 , player2 , joueur ) ;

	$( 'div.whoStarted' ).css({
		'opacity' : '0',
		'transform' : 'scale(0)',
	});

	setTimeout(function() {
		$( 'div.whoStarted' ).css({
			'z-index' : '-10000' ,
		});
	}, 500);
});

$( 'button#startedGame' ).on( 'click' , function( event ){
	var player1 = $( 'input#joueur1_input' ).val() ;
	var player2 = $( 'input#joueur2_input' ).val() ;

	$( 'label.player1_text' ).text( player1 ) ;
	$( 'label.player2_text' ).text( player2 ) ;

	if( player1 !== '' && player2 !== '' ){
		game_table.initialisation( player1 , player2 ) ;
		$( 'div.tab' ).css({
			'opacity' : '0',
			'transform' : 'scale(0)',
		});

		setTimeout(function() {
			$( 'div.tab' ).css({
				'z-index' : '-10000' ,
			});
		}, 500);
	}
});

$( 'li#restart' ).on( 'click' , function( event ){
	$( 'div.whoStarted' ).css({
		'opacity' : '1',
		'transform' : 'scale(1)',
		'z-index' : '100000000' ,
	});
});

$( 'li#newGamer' ).on( 'click' , function( event ){
	$( 'div.tab' ).css({
		'opacity' : '1',
		'transform' : 'scale(1)',
		'z-index' : '1000000' ,
	});

	game_table.reinitialis_score() ;
});

$( 'div.tab' ).css({
	'opacity' : '1',
	'transform' : 'scale(1)',
});